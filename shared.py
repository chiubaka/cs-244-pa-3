
import numpy as np

def read_data(filename, mode, func=np.mean):
    """
    Parse csv produced by initcwnd.py
    """
    ret = {}
    with open(filename) as ifs:
        for line in ifs:
            line = line.strip()
            bw, rtt, cwnd, size, latencies = line.split(",")

            latencies = [float(x) for x in latencies.split(" ")]
            latencies = func(latencies)
            if mode == "rtt":
                rtt = int(rtt)
                ret[rtt] = latencies
            elif mode == "bw":
                bw = int(float(bw) * 1000) # Kbps
                ret[bw] = latencies
            elif mode == "cwnd":
                cwnd = int(cwnd)
                ret[cwnd] = latencies
            elif mode == "size":
                size = int(size)
                ret[size] = latencies
    return ret

def process_data(origData, newData):
    """
    Computes the absolute and relative improvements
    """
    x = sorted(origData.keys())
    yAbs = []
    yRel = []

    for k in x:
        origValue = origData[k]
        newValue = newData[k]

        absDiff = newValue - origValue
        relDiff = (absDiff / origValue) * 100.0

        yAbs.append(-absDiff)
        yRel.append(-relDiff)

    return x, yAbs, yRel
