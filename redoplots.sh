#! /bin/bash

# Output and input directory
rootdir=$1
echo $rootdir

python plot_bw.py $rootdir/variable-bw-cwnd-3.csv $rootdir/variable-bw-cwnd-10.csv -d $rootdir

python plot_rtt.py $rootdir/variable-rtt-cwnd-3.csv $rootdir/variable-rtt-cwnd-10.csv -d $rootdir

python plot_size.py $rootdir/variable-size-cwnd-3.csv $rootdir/variable-size-cwnd-10.csv -d $rootdir

python plot_cwnd.py $rootdir/variable-cwnd.csv -d $rootdir
