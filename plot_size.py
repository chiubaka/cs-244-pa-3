
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from shared import read_data, process_data

parser = argparse.ArgumentParser(description="Plot absolute and relative \
    changes in latency vs. request size.")
parser.add_argument("orig", type=str)
parser.add_argument("new", type=str)
parser.add_argument("--dir", "-d", type=str, help="write the plot to png")

args = parser.parse_args()

def generate_plot(x, yAbs, yRel):
    """
    Create the plot
    """

    ind = np.arange(len(x))
    width = 0.35

    fig, axAbs = plt.subplots()
    # fig.set_facecolor("white")
    axAbs.set_yscale('log')
    axRel = axAbs.twinx()

    rectsAbs = axAbs.bar(ind, yAbs, width, color='b', log=True)
    rectsRel = axRel.bar(ind + width, yRel, width, color='r')

    axAbs.set_ylabel('Improvement (ms)')
    axAbs.set_xlabel('Request payload size (Kb)')
    axAbs.set_xticks(ind + width)
    axAbs.set_xticklabels(x)
    axAbs.set_ylim(1, 1000)
    axAbs.set_autoscale_on(False)

    yticks = mtick.FormatStrFormatter('%.0f%%')
    axRel.yaxis.set_major_formatter(yticks)
    axRel.set_ylim(0, 100)
    axRel.set_autoscale_on(False)

    axAbs.legend((rectsAbs[0], rectsRel[0]),
        ("Absolute Improvement", "Percentage Improvement"), loc=2)

    if not args.dir is None:
        plt.savefig("%s/size.png" % args.dir, dpi=100)
    else:
        plt.show()


def main():
    origData = read_data(args.orig, "size")
    newData = read_data(args.new, "size")

    x, yAbs, yRel = process_data(origData, newData)

    generate_plot(x, yAbs, yRel)

if __name__ == "__main__":
    main()
