# README #

To run, use the command: sudo ./run.sh

Results from previous runs, the with patched and unpactched kernels are
included for reference in example-results-modified-kernel and
example-results, respectively.

### Running the experiment
1. Launch a new EC2 instance. Under Community AMI’s search for
cs244-dwchiu-jamesh93-pa3 and create an instance with this image. A c3.large
instance should be more than sufficient.

    A. If the Community AMI cannot be found (possibly waiting to be indexed
    by Amazon), it should suffice to make an instance off of the cs244-Spr15-
    Mininet AMI also available under Community AMI’s

    B. You will then need to clone the repository for our code available here:
    https://chiubaka@bitbucket.org/chiubaka/cs-244-pa-3.git

2. Once your EC2 instance is up and running, SSH into it as the user: ubuntu.
3. In the home folder for the ubuntu user, there is a folder called
cs-244-pa-3. Navigate into this folder to find the experiment files.
4. Run git pull to make sure you have the most up-to-date version of the
experiment files.
5. Run sudo ./run.sh to run the experiments. Results will be output to a
timestamped results folder in the current directory.
* Please note that there is an exception raised when the script cleans up the
Mininet topology. This exception does not affect functionality of the code and
seems to be a bug related to Mininet’s interaction with the VM as was reported
in PA2.
6. Compare resulting images to the images available in the example-results
directory. Results for RTT’s at or above 1000ms may not match because the
kernel initial RTO (timeout value) is set to 1000ms by default and interferes
with the experiment. (The AMI runs the unmodified kernel, since it doesn’t
make realistic sense for an admin tuning initial cwnd at a server to configure
client settings.)