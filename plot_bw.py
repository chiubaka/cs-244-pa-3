
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from shared import read_data, process_data

parser = argparse.ArgumentParser(
    description="Plot absolute and relative changes in latency vs. bandwidth.")
parser.add_argument("orig", type=str)
parser.add_argument("new", type=str)
parser.add_argument("--dir", "-d", type=str, help="write the plot to png")

args = parser.parse_args()

def generate_plot(x, yAbs, yRel):
    """
    Create the plot
    """
    yAbs = [max(a,0.001) for a in yAbs]
    yRel = [max(a,0.001) for a in yRel]

    ind = np.arange(len(x))
    width = 0.35

    fig, axAbs = plt.subplots()
    # fig.set_facecolor("white")
    axAbs.set_yscale('log')
    axRel = axAbs.twinx()

    rectsAbs = axAbs.bar(ind, yAbs, width, color='b', log=True)
    rectsRel = axRel.bar(ind + width, yRel, width, color='r')

    axAbs.set_ylabel('Improvement (ms)')
    axAbs.set_xlabel('Bandwidth (Kbps)')
    axAbs.set_xticks(ind + width)
    axAbs.set_xticklabels(x)
    axAbs.set_ylim(1, 10000)
    axAbs.set_autoscale_on(False)

    yticks = mtick.FormatStrFormatter('%.0f%%')
    axRel.yaxis.set_major_formatter(yticks)
    axRel.set_ylim(0, 100)
    axRel.set_autoscale_on(False)

    axAbs.legend((rectsAbs[0], rectsRel[0]),
        ("Absolute Improvement", "Percentage Improvement"), loc=2)

    if not args.dir is None:
        plt.savefig("%s/bw.png" % args.dir, dpi=100)
    else:
        plt.show()


def main():
    origData = read_data(args.orig, "bw")
    newData = read_data(args.new, "bw")

    x, yAbs, yRel = process_data(origData, newData)

    generate_plot(x, yAbs, yRel)

if __name__ == "__main__":
    main()
