
import BaseHTTPServer

import argparse
import string
import random

parser = argparse.ArgumentParser()
parser.add_argument("--size", "-s", type=int, default=9,
    help="size of request body in kb")
parser.add_argument("--static", action="store_true", default=False,
    help="serve pages based on distribution")
parser.add_argument("--maxresponse", type=int, default=30 * 1024,
    help="largest response size in bytes")
parser.add_argument("--port", "-p", type=int, default=8080,
    help="port to run server")
args = parser.parse_args()

CACHED_RESPONSE = None

def getResponseLength():
    """
    Returns the length in bytes based Google Web search response
    length distribution in figure 1
    """
    p = random.random()
    if p < 0.08:
        lower = 100
        upper = 700
    elif p < 0.11:
        lower = 700
        upper = 3300
    elif p < 0.2:
        lower = 3300
        upper = 5800
    elif p < 0.4:
        lower = 5800
        upper = 9000
    elif p < 0.8:
        lower = 9000
        upper = 10000
    elif p < 0.88:
        lower = 10000
        upper = 10500
    else:
        lower = 10500
        upper = args.maxresponse

    if args.maxresponse < upper:
        upper = args.maxresponse

    if lower >= upper:
        return upper

    return random.randint(lower, upper)

class StaticHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """
    Serves constant length pages
    """
    def do_GET(self):
        """ returns random page """
        self.send_response(200)
        self.send_header("Content-Type", "text/plain")
        self.send_header("Content-Length", len(CACHED_RESPONSE))
        self.end_headers()
        self.wfile.write(CACHED_RESPONSE)

class VariableHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """
    Serves pages based on distribution
    """
    def do_GET(self):
        """ returns random page """
        n = getResponseLength()
        self.send_response(200)
        self.send_header("Content-Type", "text/plain")
        self.send_header("Content-Length", n)
        self.end_headers()
        self.wfile.write(CACHED_RESPONSE[:n])

def main(args):
    if args.static:
        handler = StaticHandler
        response_length = args.size * 1024
    else:
        handler = VariableHandler
        response_length = args.maxresponse

    global CACHED_RESPONSE
    CACHED_RESPONSE = ''.join(random.choice(string.lowercase)
            for _ in xrange(response_length))

    server = BaseHTTPServer.HTTPServer(('', args.port), handler)
    print "Serving random data serving at port", args.port
    server.serve_forever()

if __name__ == "__main__":
    main(args)

