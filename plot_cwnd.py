
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

from shared import read_data

parser = argparse.ArgumentParser(
    description="Plot latency versus initcwnd.")
parser.add_argument("data", type=str)
parser.add_argument("--dir", "-d", type=str, help="write the plot to png")

args = parser.parse_args()


def process_data(data):
    x = sorted(data.keys())
    y = [data[a] for a in x]
    return x, y

def generate_plot(x, y, ySd):
    """
    Create the plot
    """
    ind = np.arange(len(x))
    width = 0.35

    fig, ax = plt.subplots()

    rects = ax.bar(ind + width, y, width, color='r', yerr=ySd)

    ax.set_ylabel('TCP latency (ms)')
    ax.set_xlabel('init_cwnd (packets)')
    ax.set_xticks(ind + width)
    ax.set_xticklabels(x)
    yMin = (np.amin(y) / 5) * 5 - 50
    yMax = (np.amax(y) / 5) * 5 + 100
    ax.set_ylim(yMin, yMax)
    ax.set_autoscale_on(False)

    ax.legend((rects[0],), ("simulated Google Web search",), loc=1)

    if not args.dir is None:
        plt.savefig("%s/cwnd.png" % args.dir, dpi=100)
    else:
        plt.show()


def main():
    data = read_data(args.data, "cwnd")
    dataSd = read_data(args.data, "cwnd", func=np.std)
    x, y = process_data(data)
    x, ySd = process_data(dataSd)
    generate_plot(x, y, ySd)

if __name__ == "__main__":
    main()
