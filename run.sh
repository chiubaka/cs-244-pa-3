#! /bin/bash

# Exit on any failure
set -e

# Check for uninitialized variables
set -o nounset

ctrlc() {
    killall -9 python
    mn -c
    exit
}

trap ctrlc SIGINT

start=`date`
exptid=`date +%b%d-%H:%M:%S`

rootdir=results-$exptid

mkdir $rootdir

port=8080

# Number of times to measure latency
samples=3

# A value, n, greater than 0 means pages will be size n kb. Otherwise
# the page is served with a random size, which is the default.
static=9

# ---------------
# Bandwidth tests
# ---------------

for bandwidth in 0.056 0.256 0.512 1 2 3 5 7; do
    for cwnd in 3 10; do
        file=$rootdir/variable-bw-cwnd-$cwnd.csv
        dir=$rootdir/bw$bandwidth

        python initcwnd.py --bw $bandwidth --file $file --dir $dir --icwnd $cwnd -p $port -n $samples --static $static
    done
done
python plot_bw.py $rootdir/variable-bw-cwnd-3.csv $rootdir/variable-bw-cwnd-10.csv -d $rootdir

# ---------
# RTT tests
# ---------

for rtt in 20 50 100 200 500 1000 3000 5000; do
    for cwnd in 3 10; do
        file=$rootdir/variable-rtt-cwnd-$cwnd.csv
        dir=$rootdir/rtt$rtt

        python initcwnd.py --rtt $rtt --file $file --dir $dir --icwnd $cwnd -p $port -n $samples --static $static
    done
done
python plot_rtt.py $rootdir/variable-rtt-cwnd-3.csv $rootdir/variable-rtt-cwnd-10.csv -d $rootdir

# ------------------
# Request size tests
# ------------------

for size in 1 2 4 5 9 15 50 100 1000; do
    for cwnd in 3 10; do
        file=$rootdir/variable-size-cwnd-$cwnd.csv
        dir=$rootdir/size$size

        python initcwnd.py --file $file --dir $dir --icwnd $cwnd -p $port -n $samples --static $size
    done
done
python plot_size.py $rootdir/variable-size-cwnd-3.csv $rootdir/variable-size-cwnd-10.csv -d $rootdir

# ------------------
# Initial cwnd tests
# ------------------

for cwnd in 3 6 10 16 26 42; do
    file=$rootdir/variable-cwnd.csv
    dir=$rootdir/cwnd$cwnd

    python initcwnd.py -n 100 --file $file --dir $dir --icwnd $cwnd -p $port
done
python plot_cwnd.py $rootdir/variable-cwnd.csv -d $rootdir
