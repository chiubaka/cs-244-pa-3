#! /usr/bin/python

"CS 244 PA 3: Initial cwnd"

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
from argparse import ArgumentParser
from time import sleep

import os
import sys
import numpy as np
import subprocess

from termcolor import colored


DEFAULT_RWND = 65535 / 1500

# Parse arguments

parser = ArgumentParser(description="Initial cwnd tests")
parser.add_argument('--bw', '-b',
                    dest="bw",
                    type=float,
                    action="store",
                    help="Connection bandwidth between client and server in Mbps",
                    default=1.2)
parser.add_argument('--rtt', '-r',
                    dest="rtt",
                    type=float,
                    help="Round trip time between client and host in milliseconds",
                    default=70)
parser.add_argument('--dir', '-d',
                    dest="dir",
                    action="store",
                    help="Directory to store outputs",
                    default="results")
parser.add_argument('--file', '-f',
                    dest="file",
                    action="store",
                    help="File to store results",
                    default=None)
parser.add_argument('--cong', '-c',
                    dest="cong",
                    help="Congestion control algorithm to use",
                    default="cubic")
parser.add_argument('--icwnd', '-i',
                    dest="icwnd",
                    type=int,
                    action="store",
                    help="Initial congestion window size to use on the server",
                    default=10)
parser.add_argument('--rwnd',
                    dest="rwnd",
                    type=int,
                    action="store",
                    help="Initial receive window size to use on the client",
                    default=DEFAULT_RWND)
parser.add_argument('--nsamples', '-n',
                    dest="nsamples",
                    type=int,
                    help="Number of times to measure curl time",
                    default=25)
parser.add_argument('--port', '-p',
                    type=int,
                    help="Port to run web server",
                    default=8080)
parser.add_argument('--debug',
                    dest="debug",
                    action="store_true")
parser.add_argument('--debugcwnd',
                    dest="debugcwnd",
                    action="store_true")
parser.add_argument('--static',
                    type=int,
                    help="serve static web pages of size kb")

# Extract parameters
args = parser.parse_args()

class ICwndTopo(Topo):
    """
    Topology
    """
    def build(self, bw=None, rtt=None):
        """
        Initial setup
        """
        client = self.addHost('client')
        server = self.addHost('server')

        switch = self.addSwitch('s0')

        # Divide RTT by 4 to get one-hop link delay
        link_delay = str(rtt / 4.0) + "ms"

        # bottleneck link
        self.addLink(client, switch, bw=bw, delay=link_delay)
        self.addLink(server, switch, bw=1000, delay=link_delay,
            max_queue_size=1000)

def verify_latency(net):
    """
    verify latency with ping
    """
    client, server = net.get("client", "server")
    net.pingFull(hosts=(client, server))

def verify_bandwidth(net):
    """
    check the bandwidth using UDP
    """
    client, server = net.get("client", "server")
    udpBw = str(args.bw) + "M"
    net.iperf(hosts=(client, server), l4Type="UDP", udpBw=udpBw, seconds=15)

def start_tcpprobe(outfile):
    """
    Run tcp probe. From PA1.
    """
    os.system("rmmod tcp_probe; modprobe tcp_probe port=%d;" % args.port)
    subprocess.Popen("cat /proc/net/tcpprobe > %s" % outfile, shell=True)

def stop_tcpprobe():
    """
    Stop tcp probe. From PA1.
    """
    subprocess.Popen("killall -9 cat", shell=True).wait()

def verify_cwnd(net):
    """
    Confirm that initial cwnd is correctly set
    """
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)

    outfile = "%s/tcpprobe-cwnd-%d.txt" % (args.dir, args.icwnd)
    start_tcpprobe(outfile)

    client, server = net.get("client", "server")

    print "Verifying initial cwnd of client and server"
    serverProc = server.popen("iperf -s -w 16m -p %d" % args.port)
    clientProc = client.popen("iperf -c %s -p %d -t %d" % (
        server.IP(), args.port, 5 * 60))

    clientProc.wait()
    serverProc.kill()

    stop_tcpprobe()
    sleep(120)

    with open(outfile, "rb") as ifs:
        line = ifs.readline().strip()
        initialCwnd = line.split(" ")[6]
        print "*** initcwnd=%s expected=%d" % (initialCwnd, args.icwnd)

def start_webserver(net):
    """
    Starts the webserver on the server host.
    """
    server = net.get("server")
    if args.static is None or args.static <= 0:
        cmd = "python webserver.py -p %d" % args.port
    else:
        cmd = "python webserver.py --static --size %d -p %d" % (
            args.static, args.port)
        sleep(int(np.log(args.static))) # give the server some time

    print "Running command to start server: %s" % cmd
    proc = server.popen(cmd)
    sleep(1)
    return [proc]

def setup_tcpdump(net):
    """
    Run tcp dump on both client and server.
    """
    server = net.get("server")
    client = net.get("client")

    if not os.path.exists(args.dir):
        os.makedirs(args.dir)

    serverPcapFile = args.dir + ("/server-cwnd%d.pcap" % args.icwnd)
    serverCmd = "tcpdump -i %s -w %s -B 4096" % (
        server.intfNames()[0], serverPcapFile)
    server.popen(serverCmd)

    clientPcapFile = args.dir + ("/client-cwnd%d.pcap" % args.icwnd)
    clientCmd = "tcpdump -i %s -w %s -B 4096" % (
        client.intfNames()[0], clientPcapFile)
    client.popen(clientCmd)

def configure_net(net):
    server = net.get("server")
    client = net.get("client")

    # disable overzealous arp timeouts
    server.cmd("sysctl -w net.ipv4.neigh.server-eth0.gc_stale_time=3600")
    client.cmd("sysctl -w net.ipv4.neigh.client-eth0.gc_stale_time=3600")

    # set neighbor solicitation retransmissions to 1 minute
    server.cmd("sysctl -w net.ipv4.neigh.server-eth0.retrans_time_ms=60000")
    client.cmd("sysctl -w net.ipv4.neigh.client-eth0.retrans_time_ms=60000")

    server.cmd("sysctl -p")
    client.cmd("sysctl -p")

def configure_cwnd(net):
    """
    Sets up initial cwnd.
    """
    server = net.get("server")
    client = net.get("client")

    flushCacheCmd = "ip route flush cache"

    serverExistingRoute = server.cmd("ip route show").strip()
    serverCmd = "ip route change %s initrwnd %d initcwnd %d" % (
        serverExistingRoute, args.rwnd, args.icwnd)
    server.cmd(serverCmd)
    server.cmd(flushCacheCmd)

    clientExistingRoute = client.cmd("ip route show").strip()
    clientCmd = "ip route change %s initrwnd %d initcwnd %d" % (
        clientExistingRoute, args.rwnd, args.icwnd)
    client.cmd(clientCmd)
    client.cmd(flushCacheCmd)

    # print "server:", serverCmd
    # print "client:", clientCmd

def measure_webpage_transfer_time(net):
    """
    Record curl transfer time.
    """
    client, server = net.get("client", "server")

    curlCmd = "curl -o /dev/null -s -w %%{time_total} %s:%d" % (server.IP(), args.port)

    numSamples = args.nsamples
    results = np.zeros(numSamples)

    bdp = args.bw * args.rtt * 10**3
    print colored("Running cURL for bw=%.4fMbps rtt=%dms bdp=%dbits icwnd=%d" \
        % (args.bw, args.rtt, bdp, args.icwnd), "red")

    print colored("samples ->","red"),
    for i in xrange(numSamples):
        results[i] = float(client.cmd(curlCmd))
        print colored(results[i], "red"),
        sys.stdout.flush()
        sleep((args.rtt / 1000) + 1) # for greater separation in pcap
    print ""

    print colored("mean=%.4f median=%.4f stdev=%.4f" % (
        np.mean(results), np.median(results), np.std(results)), "red")

    return results

def store_results(results):
    """
    Appends results to result file if one exists.
    """
    if args.file is None:
        print "no file to store results"
        return

    if not args.static is None and args.static > 0:
        size = str(args.static)
    else:
        size = ""

    with open(args.file, "a+") as ofs:
        row = "%f,%d,%d,%s,%s" % (
            args.bw, args.rtt, args.icwnd, size, " ".join(["%f" % (x * 1000) for x in results]))
        ofs.write(row)
        ofs.write("\n")

    print "appended results to %s" % args.file

def main():
    """
    Run the experiment.
    """
    os.system("sysctl -w net.ipv4.tcp_congestion_control=%s" % args.cong)
    os.system("sysctl -p")

    # Instantiate the ICwndTopo
    topo = ICwndTopo(bw=args.bw, rtt=args.rtt)
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()

    configure_net(net)
    configure_cwnd(net)

    dumpNodeConnections(net.hosts)

    # Keep trying until connected (For absurdly large RTTs)
    # Seems to be a timeout issue with ARP
    while net.pingAll() > 0:
        pass

    # Verify the latency and bandwidth of the topo
    if args.debug:
        verify_latency(net)
        verify_bandwidth(net)

    if args.debugcwnd:
        verify_cwnd(net)

    # Log pcap files
    if args.debug:
        setup_tcpdump(net)

    # Start a webserver on the server
    start_webserver(net)

    # Use curl or wget to download a webpage from the webserver
    # several times and report the mean time

    print ""
    results = measure_webpage_transfer_time(net)
    print ""

    # Appends result to result file
    store_results(results)

    # Cleanup the topology and kill everything (kill tcpprobe)
    net.stop()
    subprocess.Popen("pgrep -f tcpdump | xargs kill -9", shell=True).wait()
    subprocess.Popen("pgrep -f webserver.py | xargs kill -9",
        shell=True).wait()

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat iperf; mn -c")
